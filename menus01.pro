#-------------------------------------------------
#
# Project created by QtCreator 2014-02-16T10:02:43
#
#-------------------------------------------------

QT       += core gui

TARGET = menus01
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

RESOURCES += \
    rc.qrc
